package in.himtech.jms.rabit.workqueue;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;
import com.rabbitmq.client.ShutdownSignalException;

public class MsgReceiver {
	private static final String TASK_QUEUE_NAME = "work_queue";

	public static void main(String[] args) {
		ConnectionFactory connectionFactory = new ConnectionFactory();
		connectionFactory.setHost("localhost");
		Connection connection = null;
		Channel channel = null;

		try {
			connection = connectionFactory.newConnection();
			channel = connection.createChannel();

			boolean durable = true;
			channel.queueDeclare(TASK_QUEUE_NAME, durable, false, false, null);

			System.out
					.println(" [*] Waiting for messages. To exit press CTRL+C");
			
			channel.basicQos(1); //deliver message only when i acknowledged.

			QueueingConsumer consumer = new QueueingConsumer(channel);
			boolean autoAck = false;
			channel.basicConsume(TASK_QUEUE_NAME, autoAck, consumer);

			while (true) {
				QueueingConsumer.Delivery delivery = consumer.nextDelivery();
				String msg = new String(delivery.getBody());
				System.out.println("Message Received: " + msg);
				doWork(msg);
				System.out.println("Processing complete: " + msg);
				channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
			}

		} catch (IOException | ShutdownSignalException | InterruptedException ex) {
			System.out.println(ex.getMessage());
			ex.printStackTrace();
		} finally {
			try {
				connection.close();
				channel.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}

	private static void doWork(String msg) throws InterruptedException {
		char[] ctrarr = msg.toCharArray();

		for (char ctr : ctrarr) {
			if (ctr == '.')
				TimeUnit.SECONDS.sleep(1);
			;
		}
	}
}
