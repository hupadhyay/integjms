package in.himtech.jms.rabit.workqueue;

import java.io.IOException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;

public class MsgSender {
	private static final String TASK_QUEUE_NAME = "work_queue";

	public static void main(String[] args) {
		ConnectionFactory connectionFactory = new ConnectionFactory();
		connectionFactory.setHost("localhost");
		Connection connection = null;
		Channel channel = null;

		try {
			connection = connectionFactory.newConnection();
			channel = connection.createChannel();
			
			boolean durable = true;
			channel.queueDeclare(TASK_QUEUE_NAME, durable, false, false, null);
			String msg = getMessage(args);
			channel.basicPublish("", TASK_QUEUE_NAME, MessageProperties.PERSISTENT_TEXT_PLAIN, msg.getBytes());
			System.out.println("Message Sent: " + msg);
		} catch (IOException ex) {
			System.out.println("Exception during sending message:");
			ex.printStackTrace();
		} finally {
			try {
				channel.close();
				connection.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

	}

	private static String getMessage(String[] args) {
		if (args.length < 1) {
			return "Hello World";
		} else {
			return combineString(args);
		}
	}

	private static String combineString(String[] args) {
		StringBuilder sb = new StringBuilder();

		for (String str1 : args) {
			sb.append(str1);
			sb.append(" ");
		}
		return sb.toString();
	}
}
