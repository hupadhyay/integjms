Rabbit MQ :

RabbitMQ is a message broker. In essence, it accepts messages from producers, 
and delivers them to consumers. In-between, it can route, buffer, and persist 
the messages according to rules you give it.

Producer:-- Producing means nothing more than sending. A program that sends messages is a producer.

Queue:-- A queue is the name for a mailbox. It lives inside RabbitMQ. Although messages flow through RabbitMQ 
and your applications, they can be stored only inside a queue. A queue is not bound by any limits, 
it can store as many messages as you like - it's essentially an infinite buffer. 
Many producers can send messages that go to one queue - many consumers can try to receive data from one queue. 

Consumer:-- Consuming has a similar meaning to receiving. A consumer is a program that mostly waits to receive messages.

Work Queue:-- Work Queue will be used to distribute time-consuming tasks among multiple workers.

Message Acknowledgement:-- once RabbitMQ delivers a message to the customer it immediately removes it from memory. 
In this case, if you kill a worker we will lose the message it was just processing. We'll also lose all the messages 
that were dispatched to this particular worker but were not yet handled.
In order to make sure a message is never lost, RabbitMQ supports message acknowledgments. An ack(nowledgement) 
is sent back from the consumer to tell RabbitMQ that a particular message has been received, processed and that 
RabbitMQ is free to delete it.

Message Durability:-- We have learned how to make sure that even if the consumer dies, the task isn't lost. 
But our tasks will still be lost if RabbitMQ server stops.
When RabbitMQ quits or crashes it will forget the queues and messages unless you tell it not to. Two things are required to 
make sure that messages aren't lost: we need to mark both the queue and messages as durable.

Exchange: fanout exchange only capable of mindless broadcasting. It broadcast to all of the registered queues. Whereas "direct"
exchange is capable of 

