package in.himtech.jms.rabit.spring;

import in.himtech.jms.post.BackOffice;
import in.himtech.jms.post.Mail;

import org.springframework.amqp.rabbit.core.support.RabbitGatewaySupport;

public class BackDeskImpl extends RabbitGatewaySupport implements BackOffice {

	@Override
	public Mail receiveMail() {
		Mail mail = (Mail)getRabbitTemplate().receiveAndConvert();

		return mail;
	}

}
