package in.himtech.jms.rabit.spring;

import in.himtech.jms.post.FrontOffice;
import in.himtech.jms.post.Mail;

import org.springframework.amqp.rabbit.core.support.RabbitGatewaySupport;

public class FrontDeskImpl extends RabbitGatewaySupport implements FrontOffice {

	@Override
	public void sendmail(Mail mail) {
		getRabbitTemplate().convertAndSend(mail);

	}

}
