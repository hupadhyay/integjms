package in.himtech.jms.rabit.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

public class FrontMain {

	public static void main(String[] args) {
		ApplicationContext context = new GenericXmlApplicationContext(
				"classpath:rabbitmq/SenderContext.xml");
		
		
		
		((ConfigurableApplicationContext)context).close();
	}
}
