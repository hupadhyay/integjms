package in.himtech.jms.rabit.routing;

import java.io.IOException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.ConsumerCancelledException;
import com.rabbitmq.client.QueueingConsumer;
import com.rabbitmq.client.ShutdownSignalException;

public class ReceiveLogDirect {
	private static String DIRECT_EXCHANGE = "DIRECT_EXCHANGE";
	
	public static void main(String[] args) {
		ConnectionFactory connectionFactory = new ConnectionFactory();
		connectionFactory.setHost("localhost");
		Connection connection = null;
		Channel channel = null;
		
		try{
			connection = connectionFactory.newConnection();
			channel = connection.createChannel();
			
			channel.exchangeDeclare(DIRECT_EXCHANGE, "direct");
			channel.queueDeclare("tempQueue2", false, false, false, null);
			
			
			channel.queueBind("tempQueue2", DIRECT_EXCHANGE, "red");
			channel.queueBind("tempQueue2", DIRECT_EXCHANGE, "blue");
			
			System.out.println("Waiting for message........(to exit press ctrl + X)");
			
			QueueingConsumer consumer = new QueueingConsumer(channel);
			channel.basicConsume("tempQueue2", true, consumer);

			while(true){
				QueueingConsumer.Delivery delivery = consumer.nextDelivery();
				String message = new String(delivery.getBody());
				String routingKey = delivery.getEnvelope().getRoutingKey();
				System.out.println("Received Message: " +  message + ", " + routingKey);
			}
			
		} catch(IOException | ShutdownSignalException | ConsumerCancelledException | InterruptedException exp){
			System.out.println(exp.getMessage());
			exp.printStackTrace();
		} finally{
			try {
				channel.close();
				connection.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}

	}
}
