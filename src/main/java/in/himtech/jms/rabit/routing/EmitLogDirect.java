package in.himtech.jms.rabit.routing;

import java.io.IOException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class EmitLogDirect {
	private static String DIRECT_EXCHANGE = "DIRECT_EXCHANGE";

	public static void main(String[] args) {
		ConnectionFactory connectionFactory = new ConnectionFactory();
		connectionFactory.setHost("localhost");
		Connection connection = null;
		Channel channel = null;

		try {
			connection = connectionFactory.newConnection();
			channel = connection.createChannel();
			
			channel.exchangeDeclare(DIRECT_EXCHANGE, "direct");
			
			String msg = "Ghanta Message with red";
			String severity = "red";
			
			channel.basicPublish(DIRECT_EXCHANGE, severity, null, msg.getBytes());
			System.out.println("Message Sent: " + severity + " : " + msg);
			
			channel.close();
			connection.close();
		} catch (IOException ex) {
			System.out.println(ex.getMessage());
		} 
	}
}
