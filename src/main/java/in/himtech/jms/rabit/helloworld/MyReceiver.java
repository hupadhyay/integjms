package in.himtech.jms.rabit.helloworld;

import java.io.IOException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.ConsumerCancelledException;
import com.rabbitmq.client.QueueingConsumer;
import com.rabbitmq.client.ShutdownSignalException;

public class MyReceiver {
	public static void main(String[] args) {
		ConnectionFactory connectionFactory = new ConnectionFactory();
		connectionFactory.setHost("localhost");
		Connection connection = null;
		Channel channel = null;
		
		try {
			connection = connectionFactory.newConnection();
			channel = connection.createChannel();
			channel.queueDeclare("msgQueue", false, false, false, null);
			
			System.out.println(" [*] Waiting for messages. To exit press CTRL+C");
			
			QueueingConsumer consumer = new QueueingConsumer(channel);
			channel.basicConsume("msgQueue", true, consumer);
			
			while(true){
				QueueingConsumer.Delivery delivery = consumer.nextDelivery();
				String msg = new String(delivery.getBody());
				System.out.println("Message Received: " + msg);
			}
			
		} catch (IOException ex) {
			System.out.println("IOException");
			ex.printStackTrace();
		} catch (ShutdownSignalException e) {
			System.out.println("ShutdownSignalException");
			e.printStackTrace();
		} catch (ConsumerCancelledException e) {
			System.out.println("ConsumerCancelledException");
			e.printStackTrace();
		} catch (InterruptedException e) {
			System.out.println("InterruptedException");
			e.printStackTrace();
		}finally{
			try {
				channel.close();
				connection.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
}
