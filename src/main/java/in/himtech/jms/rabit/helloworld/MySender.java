package in.himtech.jms.rabit.helloworld;

import java.io.IOException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class MySender {
	public static void main(String[] args) {
		ConnectionFactory connectionFactory = new ConnectionFactory();
		connectionFactory.setHost("localhost");
		Connection connection = null;
		Channel channel = null;
		
		try {
			connection = connectionFactory.newConnection();
			channel = connection.createChannel();
			channel.queueDeclare("msgQueue", false, false, false, null);
			String myMsg = "Hrishik and Hritika";
			channel.basicPublish("", "msgQueue", null, myMsg.getBytes());
			System.out.println("Message Sent");
		} catch (IOException ex) {
			ex.printStackTrace();
		}finally{
			try {
				channel.close();
				connection.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
}
