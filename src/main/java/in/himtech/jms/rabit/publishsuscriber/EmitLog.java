package in.himtech.jms.rabit.publishsuscriber;

import java.io.IOException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class EmitLog {

	private final static String EXCHANGE_NAME = "logExchange";

	public static void main(String[] args) {
		ConnectionFactory connectionFactory = new ConnectionFactory();
		connectionFactory.setHost("localhost");
		Connection connection = null;
		Channel channel = null;

		try {
			connection = connectionFactory.newConnection();
			channel = connection.createChannel();
			
			channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
			
			String str = "My Message will deliver to all queue.";
			channel.basicPublish(EXCHANGE_NAME, "", null, str.getBytes());
			System.out.println("Message Sent to Exchange: " + EXCHANGE_NAME );
			System.out.println("Message: " + str);
			
		} catch (IOException ex) {
			System.out.println(ex.getMessage());
			ex.printStackTrace();
		} finally {
			try {
				channel.close();
				connection.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
}
