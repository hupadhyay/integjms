package in.himtech.jms.post;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.Session;

import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

public class FrontDeskImpl implements FrontOffice {

	private JmsTemplate jmsTemplate;
//	private Destination destination;
	
	public void setJmsTemplate(JmsTemplate jmsTemplate) {
		this.jmsTemplate = jmsTemplate;
	}
	
//	public void setDestination(Destination destination) {
//		this.destination = destination;
//	}
	
	@Override
	public void sendmail(final Mail mail) {
		jmsTemplate.send(new MessageCreator() {
			
			@Override
			public Message createMessage(Session session) throws JMSException {
				MapMessage mapMessage = session.createMapMessage();
				mapMessage.setString("mailID", mail.getMailId());
				mapMessage.setString("country", mail.getCountry());
				mapMessage.setDouble("weight", mail.getWeight());
				return mapMessage;
			}
		});

	}

}
