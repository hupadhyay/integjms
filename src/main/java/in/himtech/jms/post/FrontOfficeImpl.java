package in.himtech.jms.post;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.MessageProducer;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;


public class FrontOfficeImpl implements FrontOffice {

	@Override
	public void sendmail(Mail mail) {
		ConnectionFactory cf = new ActiveMQConnectionFactory("tcp://localhost:61616");
		Destination dest = new ActiveMQQueue("mail.queue");
		
		Connection conn = null;
		
		try{
			conn = cf.createConnection();
			Session session = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
			MessageProducer producer = session.createProducer(dest);
			
			MapMessage mapMessage = session.createMapMessage();
			mapMessage.setString("mailId", mail.getMailId());
			mapMessage.setString("country", mail.getCountry());
			mapMessage.setDouble("weight", mail.getWeight());
			
			producer.send(mapMessage);
			session.close();
		}catch(JMSException jmse){
			jmse.printStackTrace();
		}finally{
			if(conn != null){
				try{
					conn.close();
				}catch(JMSException jmse){
					System.out.println(jmse.getMessage());
				}
			}
		}
	}

}
