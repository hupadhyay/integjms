package in.himtech.jms.post.converter;

import in.himtech.jms.post.BackOffice;
import in.himtech.jms.post.Mail;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

public class BackMain {

	public static void main(String[] args) {

		ApplicationContext context = new GenericXmlApplicationContext(
				"classpath:jmsconverter/ReceiverContext.xml");
	
		BackOffice backOffice = context.getBean(BackDeskImpl.class);	
		Mail mail = backOffice.receiveMail();
		System.out.println(mail);
		
		((ConfigurableApplicationContext)context).close();
	}

}
