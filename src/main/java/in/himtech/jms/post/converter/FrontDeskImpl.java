package in.himtech.jms.post.converter;

import in.himtech.jms.post.FrontOffice;
import in.himtech.jms.post.Mail;

import java.util.HashMap;
import java.util.Map;

import org.springframework.jms.core.support.JmsGatewaySupport;

public class FrontDeskImpl extends JmsGatewaySupport implements FrontOffice {
	
	@Override
	public void sendmail(Mail mail) {
		Map<String, Object>map = new HashMap<String, Object>();
		map.put("mailId", mail.getMailId());
		map.put("country", mail.getCountry());
		map.put("weight", mail.getWeight());
		
		getJmsTemplate().convertAndSend(map);
	}
}
