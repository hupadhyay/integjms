package in.himtech.jms.post.converter;

import in.himtech.jms.post.FrontOffice;
import in.himtech.jms.post.Mail;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

public class FrontMain {

	public static void main(String[] args) {
		ApplicationContext context = new GenericXmlApplicationContext(
				"classpath:jmsconverter/SenderContext.xml");
		
		FrontOffice frontOffice = context.getBean(FrontDeskImpl.class);
		Mail mail = new Mail("M003", "India", 5.45);
		frontOffice.sendmail(mail);
		
		((ConfigurableApplicationContext)context).close();
	}

}
