package in.himtech.jms.post.converter;

import in.himtech.jms.post.BackOffice;
import in.himtech.jms.post.Mail;

import java.util.Map;

import org.springframework.jms.core.support.JmsGatewaySupport;

public class BackDeskImpl extends JmsGatewaySupport implements BackOffice {

	@Override
	public Mail receiveMail() {
		Map map = (Map) getJmsTemplate().receiveAndConvert();

		if (map != null) {
			Mail mail = new Mail();
			mail.setCountry(map.get("country").toString());
			mail.setMailId(map.get("mailId").toString());
			mail.setWeight(Double.parseDouble(map.get("weight").toString()));
			return mail;
		}
		return null;
	}

}
