package in.himtech.jms.post;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MapMessage;

import org.springframework.jms.core.JmsTemplate;

public class BackDeskImpl implements BackOffice {

	private JmsTemplate jmsTemplate;
//	private Destination destination;

	public void setJmsTemplate(JmsTemplate jmsTemplate) {
		this.jmsTemplate = jmsTemplate;
	}

//	public void setDestination(Destination destination) {
//		this.destination = destination;
//	}

	@Override
	public Mail receiveMail() {
		MapMessage mapMessage = (MapMessage) jmsTemplate.receive();

		try {
			if (mapMessage != null) {
				Mail mail = new Mail();
				mail.setCountry(mapMessage.getString("country"));
				mail.setMailId(mapMessage.getString("mailID"));
				mail.setWeight(mapMessage.getDouble("weight"));
				return mail;
			}
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

}
