package in.himtech.jms.post;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

public class FrontMain {

	public static void main(String[] args) {
//		FrontOffice frontOffice = new FrontOfficeImpl();
//		Mail mail = new Mail("MYMAIL001", "INDIA", 1.25);
//		frontOffice.sendmail(mail);

		ApplicationContext context = new GenericXmlApplicationContext(
				"classpath:SenderContext.xml");
		
		FrontOffice frontOffice = context.getBean(FrontDeskImpl.class);
		Mail mail = new Mail("M003", "India", 5.45);
		frontOffice.sendmail(mail);
		
		((ConfigurableApplicationContext)context).close();
	}

}
