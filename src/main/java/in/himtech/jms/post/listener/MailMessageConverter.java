package in.himtech.jms.post.listener;

import in.himtech.jms.post.Mail;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.Session;

import org.springframework.jms.support.converter.MessageConversionException;
import org.springframework.jms.support.converter.MessageConverter;

public class MailMessageConverter implements MessageConverter {

	@Override
	public Object fromMessage(Message message) throws JMSException,
			MessageConversionException {
		MapMessage mapMessage = (MapMessage)message;
		Mail mail = new Mail();
		mail.setMailId(mapMessage.getString("mailId"));
		mail.setCountry(mapMessage.getString("country"));
		mail.setWeight(mapMessage.getDouble("weight"));
		
		return mail;
	}

	@Override
	public Message toMessage(Object mailObj, Session session) throws JMSException,
			MessageConversionException {
		MapMessage mapMessage = session.createMapMessage();
		
		Mail mail = (Mail)mailObj;
		mapMessage.setString("mailId", mail.getMailId());
		mapMessage.setString("mailId", mail.getCountry());
		mapMessage.setDouble("weight", mail.getWeight());
		
		return mapMessage;
	}

}
