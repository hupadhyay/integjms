package in.himtech.jms.post.listener;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

public class BackMain {

	public static void main(String[] args) {

		//Only start IOC container is sufficient. Listener will consume message for us.
//		ApplicationContext context = new GenericXmlApplicationContext(
//				"classpath:listener/ReceiverContext.xml");
		
		ApplicationContext context1 = new GenericXmlApplicationContext(
				"classpath:listener/AdapterReceiverContext.xml");
	
		
//		((ConfigurableApplicationContext)context).close();
	}

}
