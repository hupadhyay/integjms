package in.himtech.jms.post.listener;

import in.himtech.jms.post.Mail;

import java.util.Map;

/**
 * This class will use as a target class when we declare Message Adaptor in
 * application context xml file.
 * 
 * @author himanshu
 *
 */
public class MailListener1 {
	
	public void displayMail(Mail mail){
		System.out.println(mail);
	}

}
