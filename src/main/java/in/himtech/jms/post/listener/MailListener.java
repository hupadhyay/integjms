package in.himtech.jms.post.listener;

import in.himtech.jms.post.Mail;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;

import org.springframework.jms.support.JmsUtils;

public class MailListener implements MessageListener{

	@Override
	public void onMessage(Message message) {
		MapMessage mapMessage = (MapMessage)message;
		
		try{
			Mail mail = new Mail();
			mail.setMailId(mapMessage.getString("mailId"));
			mail.setCountry(mapMessage.getString("country"));
			mail.setWeight(mapMessage.getDouble("weight"));
			displayMail(mail);
		}catch(JMSException exp){
			throw JmsUtils.convertJmsAccessException(exp);
		}
		
	}

	private void displayMail(Mail mail) {
		System.out.println(mail);
	}

}
