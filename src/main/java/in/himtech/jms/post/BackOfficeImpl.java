package in.himtech.jms.post;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;

public class BackOfficeImpl implements BackOffice {

	@Override
	public Mail receiveMail() {

		ConnectionFactory cf = new ActiveMQConnectionFactory(
				"tcp://localhost:61616");
		Destination dest = new ActiveMQQueue("mail.queue");

		Connection conn = null;

		try {
			conn = cf.createConnection();
			Session session = conn.createSession(false,
					Session.AUTO_ACKNOWLEDGE);
			MessageConsumer consumer = session.createConsumer(dest);
			
			conn.start();
			MapMessage mapMessage = (MapMessage)consumer.receive();
			
			Mail mail = new Mail();
			mail.setMailId(mapMessage.getString("mailId"));
			mail.setCountry(mapMessage.getString("country"));
			mail.setWeight(mapMessage.getDouble("weight"));
		
			session.close();
			return mail;
		} catch (JMSException jmse) {
			jmse.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (JMSException jmse) {
					System.out.println(jmse.getMessage());
				}
			}
		}
		
		return null;
	}

}
