package in.himtech.jms.post;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

public class BackMain {

	public static void main(String[] args) {
		// BackOffice backOffice = new BackOfficeImpl();
		// Mail mail = backOffice.receiveMail();
		// System.out.println(mail + " is received");

		ApplicationContext context = new GenericXmlApplicationContext(
				"classpath:ReceiverContext.xml");
	
		BackOffice backOffice = context.getBean(BackDeskImpl.class);	
		Mail mail = backOffice.receiveMail();
		System.out.println(mail);
		
		((ConfigurableApplicationContext)context).close();
	}

}
